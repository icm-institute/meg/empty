#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import pandas as pd
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
    
#class flist(pd.DataFrame):
#    def __init__(self, *args, **kw):
#        super(flist, self).__init__(*args, **kw)
#        self.__current = 0
#        
#    def __iter__(self):
#        return self.iterrows()
#
#    def next(self): # Python 3: def __next__(self)
#        if self.__current > self.shape[0]:
#            raise StopIteration
#        else:
#            self.__current += 1
#            return self.__current - 1
        
def flister(pat = '.*', rootdir = os.curdir, recurse = True):
    """
    Listing files, splitting tokens in names.
    
        flister(pat, rootdir=os.curdir)
        
        will look for pattern pat in all filenames below rootdir and return a pandas dataframe
        with columns 'fullname', 'filename', 'bytes', 'isdir', as well as any number of other columns 
        based on tokens found in pat.
        
        pat is a regular expression. Tokens are defined as alternative patterns in brackets.
        For instance:
            pat = '.*(Left|Right).txt'
            f = flister(pat)
        
        will list all files ending with Left.txt or Right.txt below the current directory.
        The file names are returned in a column of f. Another column called 'Left_Right'
        contains a match for each file.
        
        f can then be queried to isolate only e.g. *Left.txt files like this:
            f.query('Left_Right=="Left"')
        
        tokens can be named explicitly with the official regular expression syntax:
            pat = '.*(?P<orientation>Left|Right).txt'
            f = flister(pat)
            f.query('orientation=="Left"')
        
        Several tokens can be specified:
            pat = '.*(?P<color>.*)_(?P<orientation>Left|Right).txt'
            f = flister(pat)
            f.query('color== "Blue" & orientation=="Left"')
            
        Iterating over filenames is done like this:
            pat = '.*(?P<color>.*)_(?P<orientation>Left|Right).txt'
            fs = flister(pat)
            for i, f in fs.query('color== "Blue" & orientation=="Left"').iterrows():
                print('index is' + i)
                print('other attributes ', f.orientation,f.color)
        
        
    
    """
    
    import re
    pat = re.sub('\(\??\<','(?P<',pat)
    cols = re.findall('<(?P<groups>[^>]+)>',pat)
    if not(cols):
        # autoname groups
        gp = re.findall('\(([^?][^)]+)\)',pat)
        ngp = [re.sub('\W','_',g) for g in gp]
        ngp = [re.sub('_+','_',g) for g in ngp]
        ngp = [re.sub('^_','',g) for g in ngp]
        ngp = [re.sub('_$','',g) for g in ngp]
        gpr = [re.escape(g) for g in gp]
        for i in range(len(gp)):
            pat = re.sub('\(' + gpr[i] + '\)','(?P<' + ngp[i] + '>' + gp[i] + ')',pat)
        # remove bad characters
        cols = re.findall('<(?P<groups>[^>]+)>',pat)
    gps = list(cols)
    cols.append('fullname')
    cols.append('filename')
    [cols.append(col) for col in['bytes','isdir']]
    out = pd.DataFrame(columns=cols)
    
    i_f = 0
    for (r, p, f) in os.walk(rootdir):
        for ff in p+f:
            fl = os.path.join(r,ff)
#            print(fl)
            res = re.search(pat,fl)
            if res is None:
                continue
            for g in gps:
                out.loc[i_f,[g]] = res.group(g)   
            out.loc[i_f,'bytes'] = os.stat(fl).st_size
            out.loc[i_f,'isdir'] = os.path.isdir(fl)
            out.loc[i_f,'fullname'] = fl
            out.loc[i_f,'filename'] = ff
            i_f += 1
        if not recurse:
            break
    if gps:
        out = out.sort_values(by=gps)
    out = out.reset_index().drop(labels='index', axis=1)
    
    return out
    

        
        
if __name__ == '__main__':
    fs = flister('.*')#'(?P<y>\d{4})(?P<m>\d{2})(?P<d>\d{2})_(?P<h>\d{6})\(?(?P<ex>\d{1,2})?\)?\.(?P<format>.*)')
    print(fs)
    for f in fs:
        f
    #flister('.*(?P<cond>Target|RT)-epo.fif')flister('(?P<m>\d{4})(?P<m>\d{2})(?P<d>\d{2})_(?P<h>\d{6})\(?(?P<ex>\d{1,2})?\)?\.(?P<format>.*)')
    print('done.')
