%%
e_setup

% dir_orig = fullfile(dir_deriv,'maxfilter');
% dir_output = fullfile(dir_deriv,'maxfilter_spec');
dir_orig = fullfile(dir_deriv,'mne_maxwell');
dir_output = fullfile(dir_deriv,'mne_maxwell_spec');

mymkdir(dir_output)

%%
alls = flister('(?<sub>sub-[A-Za-z0-9]+)/(?<ses>ses-[A-Za-z0-9]+)/(?<mod>[A-Za-z0-9]+)/\k<sub>_\k<ses>_(?<task>task-[A-Za-z0-9]+)_(?<proc>proc-[A-Za-z0-9]+)_\k<mod>.fif','dir',dir_orig);
% alls = flister('.*.fif','dir',dir_orig);
f_toproc = {alls.name};

% disp([num2str(numel(f_toproc)) ' files to process...'])
%%
for i_f = 1:numel(f_toproc)
    f_in = f_toproc{i_f};
    dir_in = fileparts(f_in);
    dir_out = strrep(dir_in,dir_orig,dir_output);
    mymkdir(dir_out)
    
    f_out = strrep(regexprep(f_in,'_proc-tsss(nobad)?_meg.fif','_proc-spec$1_meg.mat'),dir_in,dir_out);
    if exist(f_out,'file')
        continue
    end
    disp(['############## ' f_toproc{i_f} ' #############'])
    % read data
    cfg = [];
    cfg.dataset = f_in;
    cfg.layout                  = 'neuromag306all.lay';
    hdr                         = ft_read_header(cfg.dataset);
    % take at max 300,000 samples
    cfg.trl                     = [1 min(hdr.nSamples,300000) 0];
    cfg.channel                 = 'meg';
    data                        = ft_preprocessing(cfg);
    
%     % interpolate missing channels
%     
%     todo
    
    % separate mag and grad data
    cfg = [];
    cfg.channel                 = 'megmag';
    data_mag                    = ft_preprocessing(cfg,data);
    cfg.channel                 = 'meggrad';
    data_grad                   = ft_preprocessing(cfg,data);
    
    % config to compute pow spec
    cfg                         = [];
    cfg.method                  = 'mtmfft';
    cfg.output                  = 'pow';
    cfg.foilim                  = [.1 110];
    cfg.taper                   = 'hanning';
    
    spec_mag                = ft_freqanalysis(cfg,data_mag);
    spec_mag.logpowspctrm   = log(spec_mag.powspctrm);
    spec_grad                = ft_freqanalysis(cfg,data_grad);
    spec_grad.logpowspctrm   = log(spec_grad.powspctrm);
    
    save(f_out,'spec_mag','spec_grad')    
end