#!/bin/bash

cd /network/lustre/iss02/cenir/analyse/meeg/EMPTY/code


log="../dailyscript_logs/`date +"%Y-%m-%d_%H%M".log`"

newgrp l_cenir_meeg_empty << EONG

umask 022

touch $log

echo \######## >> $log
cat /etc/hostname >> $log
echo \######## >> $log
echo >> $log
echo >> $log


. "/network/lustre/iss02/cenir/software/meeg/anaconda3/etc/profile.d/conda.sh"

conda activate mne_dev
echo \#\#\# `date +"%Y-%m-%d_%H%M"\ BIDSimport` >> $log 2>&1
python e_00_BIDSimport.py >> $log 2>&1

echo \#\#\# `date +"%Y-%m-%d_%H%M"\ maxfilter` >> $log 2>&1 
python e_00_mne_maxwell.py >> $log 2>&1 

module load MATLAB

echo \#\#\# `date +"%Y-%m-%d_%H%M"\ spectrograms` >> $log 2>&1
matlab -nodisplay -r "cd /network/lustre/iss02/cenir/analyse/meeg/EMPTY/code;e_01_compute_spec; quit" >> $log 2>&1

echo \#\#\# `date +"%Y-%m-%d_%H%M"\ dateplot` >> $log 2>&1
matlab -nosplash -nodesktop -r "cd /network/lustre/iss02/cenir/analyse/meeg/EMPTY/code;e_02_dateplot; quit" >> $log 2>&1

EONG
