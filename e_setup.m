% list all useful directories
% setup path
% list BIDS files

dir_root = '/network/lustre/iss02/cenir/analyse/meeg/EMPTY';

dir_bids = fullfile(dir_root,'/BIDS');
dir_code = fullfile(dir_root,'/code');
dir_docs = fullfile(dir_root,'/diagnostics');
dir_bin = '/network/lustre/iss02/cenir/software/meeg/neuromag/bin/util/';
dir_deriv = fullfile(dir_root,'/derivatives');


addpath(fullfile(dir_code,'MatlabHelpers'))
addpath(fullfile(dir_code,'bids-matlab'))
addpath(fullfile(dir_code,'fieldtrip'))
ft_defaults

