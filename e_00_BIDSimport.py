# This script imports all fif files found in sourcedata and pushes them to the BIDS folder
#
# fif files should have the following format :
#     sub-Paris_ses-DATEMANIP_task-EmptyRoom_meg.fif
# with DATEMANIP = date with format YYMMDD followed by MANIP name (e.g. 220826ONSTIM)
#
# BIDS folder is stored in variable bidsroot
#
# written by Maximilien Chaumon 2022. Features/bug requests to maximilien.chaumon@icm-institute.org


import os
import os.path as op
import shutil
from pathlib import Path

import mne
import mne_bids
from genericpath import exists

from flister import flister

orig = Path('/network/lustre/iss02/cenir/analyse/meeg/EMPTY/sourcedata')
bidsroot = Path('/network/lustre/iss02/cenir/analyse/meeg/EMPTY/BIDS')

# fs = flister(pat='sub-(?P<subject>[A-Za-z0-9]+)_ses-(?P<session>[A-Za-z0-9]+)_task-(?P<task>[A-Za-z0-9]+)_(?P<modality>[A-Za-z0-9]+)\.(?P<extension>[A-Za-z0-9]+)',
#         rootdir=orig, recurse=False)
fs = flister(pat='sub-(?P<subject>Paris)_ses-(?P<session>[A-Za-z0-9]+)_task-(?P<task>EmptyRoom)_(?P<modality>meg)\.(?P<extension>fif)',
             rootdir=orig, recurse=False)

for f in fs.iterrows():
    f = f[1]

    bidspath = mne_bids.BIDSPath(subject=f.subject,
                                 session=f.session,
                                 task=f.task,
                                 root=bidsroot,
                                 suffix=f.modality)
    # print(bidspath)
    try:
        tst = op.exists(bidspath.fpath)
    except:
        tst = False
    if tst:
        continue
    os.makedirs(bidspath.directory, exist_ok=True)
    print(f.fullname)

    sss_dir = op.join(op.dirname(os.readlink(f.fullname)), 'sss_config')
    if exists(sss_dir):
        b = bidspath.copy().update(acquisition='crosstalk', extension='fif')
        ct = op.join(sss_dir, 'ct_sparse.fif')
        shutil.copyfile(ct, b.fpath)
        b = bidspath.copy().update(acquisition='calibration', extension='dat')
        cal = op.join(sss_dir, 'sss_cal.dat')
        shutil.copyfile(cal, b.fpath)

    raw = mne.io.read_raw_fif(fname=f.fullname, allow_maxshield=True)
    raw.info['line_freq'] = 50
    raw.set_meas_date(f.session[0:11])

    mne_bids.write_raw_bids(raw, bids_path=bidspath, overwrite=True)
    os.rename(f.fullname, f.fullname.replace('sourcedata', 'sourcedata/done'))

print('BIDS Import - Done!')
