% create a diagnostics plot for the past N days by calling date_plot
% store it in docs/diagnostics_YYYYMMDDHHMMSS_60, where YYYYMMDD is the
% date  and time of the last recording.

N = 30;

e_setup

date_plot(datestr(now,'yyyymmdd'),N)


