# This script runs MNE's maxwell filter on any new data found in the BIDS folder dirBIDS
# and stores the resulting file in dirDeriv.
#
# written by Maximilien Chaumon 2022. Features/bug requests to maximilien.chaumon@icm-institute.org

from mne_bids import *
import mne
from flister import flister
import os
import os.path as op
from pathlib import Path
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd
import shutil

dirBIDS = Path('/network/lustre/iss02/cenir/analyse/meeg/EMPTY/BIDS')
dirDeriv = Path('/network/lustre/iss02/cenir/analyse/meeg/EMPTY/derivatives/mne_maxwell')

def plotbads(noisy_ch, flat_ch, auto_scores, ch_type='grad'):
        ch_subset = auto_scores['ch_types'] == ch_type
        ch_names = auto_scores['ch_names'][ch_subset]
        bad_ch = list(set(noisy_ch + flat_ch))
        if bad_ch:
            ch_names = [chn if chn in bad_ch else '' for chn in ch_names]
        scores = auto_scores['scores_noisy'][ch_subset]
        limits = auto_scores['limits_noisy'][ch_subset]
        bins = auto_scores['bins']  # The the windows that were evaluated.
        # We will label each segment by its start and stop time, with up to 3
        # digits before and 3 digits after the decimal place (1 ms precision).
        bin_labels = [f'{start:3.3f} – {stop:3.3f}'
                    for start, stop in bins]

        # We store the data in a Pandas DataFrame. The seaborn heatmap function
        # we will call below will then be able to automatically assign the correct
        # labels to all axes.
        data_to_plot = pd.DataFrame(data=scores,
                                    columns=pd.Index(bin_labels, name='Time (s)'),
                                    index=pd.Index(ch_names, name='Channel'))

        # First, plot the "raw" scores.
        fig, ax = plt.subplots(1, 2, figsize=(12, 8))
        fig.suptitle(f'Automated noisy channel detection: {ch_type}',
                    fontsize=16, fontweight='bold')
        sns.heatmap(data=data_to_plot, cmap='Reds', cbar_kws=dict(label='Score'),
                    ax=ax[0])
        [ax[0].axvline(x, ls='dashed', lw=0.25, dashes=(25, 15), color='gray')
            for x in range(1, len(bins))]
        ax[0].set_title('All Scores', fontweight='bold')

        # Now, adjust the color range to highlight segments that exceeded the limit.
        sns.heatmap(data=data_to_plot,
                    vmin=np.nanmin(limits),  # bads in input data have NaN limits
                    cmap='Reds', cbar_kws=dict(label='Score'), ax=ax[1])
        [ax[1].axvline(x, ls='dashed', lw=0.25, dashes=(25, 15), color='gray')
            for x in range(1, len(bins))]
        ax[1].set_title('Scores > Limit', fontweight='bold')

        # The figure title should not overlap with the subplots.
        fig.tight_layout(rect=[0, 0.03, 1, 0.95])
        plt.savefig(str(bidspath_proc.fpath).replace('_meg.fif','_bad' + ch_type + '.png'))


# print(make_report(dirBIDS))
fs = flister(pat='sub-(?P<subject>[A-Za-z0-9]+)_ses-(?P<session>[A-Za-z0-9]+)_task-(?P<task>[A-Za-z0-9]+)_(?P<modality>[A-Za-z0-9]+)(?P<extension>\.[A-Za-z0-9]+)',
        rootdir=dirBIDS, recurse=True)
fs = fs[(fs.modality == 'meg') & (fs.extension == '.fif')]

for f in fs.iterrows():
        f = f[1]

        # print(f.fullname)
        bidspath_orig = BIDSPath(subject=f.subject,
                                     session=f.session,
                                     task=f.task,
                                     root=dirBIDS,
                                     datatype=f.modality,
                                     suffix=f.modality,
                                     extension='.fif')
        
        bidspath_proc = bidspath_orig.copy().update(root=dirDeriv, processing='tsss')
        bidspath_proc_nobad = bidspath_proc.copy().update(processing='tsssnobad')
        bidspath_chans_orig = bidspath_orig.copy().update(suffix='channels',extension='.tsv')
        bidspath_chans_out = bidspath_chans_orig.copy().update(root=dirDeriv)
        
        if op.exists(bidspath_proc.fpath) and op.exists(bidspath_proc_nobad.fpath) and op.exists(bidspath_chans_out.fpath):
            continue
        else:
            shutil.rmtree(bidspath_proc.directory, ignore_errors=True)
            # os.remove(bidspath_proc.fpath) if op.exists(bidspath_proc.fpath) else None
            # os.remove(bidspath_proc_nobad.fpath) if op.exists(bidspath_proc_nobad.fpath) else None
            
        print(bidspath_orig)
        os.makedirs(bidspath_proc.directory, exist_ok=True)
        
        raw = mne.io.read_raw_fif(fname=bidspath_orig.fpath, allow_maxshield=True)
        raw.crop(tmax=min(180,np.max(raw.times)))
        
        noisy_ch, flat_ch, auto_scores = mne.preprocessing.find_bad_channels_maxwell(raw, limit=7, duration=5,
                                                    min_count=5, return_scores=True,
                                                    origin='auto', int_order=8,
                                                    ext_order=3,
                                                    calibration=bidspath_orig.meg_calibration_fpath,
                                                    cross_talk=bidspath_orig.meg_crosstalk_fpath,
                                                    coord_frame='meg',
                                                    regularize='in',
                                                    ignore_ref=False,
                                                    bad_condition='error',
                                                    head_pos=None,
                                                    mag_scale=100,
                                                    skip_by_annotation=('edge', 'bad_acq_skip'), 
                                                    h_freq=40, extended_proj=(),
                                                    verbose=True)
        
        # code from https://mne.tools/stable/auto_tutorials/preprocessing/60_maxwell_filtering_sss.html
        
        # Select the data for gradiometer channels.
        ch_type = 'grad'
        plotbads(noisy_ch, flat_ch, auto_scores, ch_type)
        ch_type = 'mag'
        plotbads(noisy_ch, flat_ch, auto_scores, ch_type)
        
        
        chans = pd.read_csv(bidspath_chans_orig.fpath,sep='\t')
        chans.loc[chans["name"].isin(noisy_ch), "status"] = 'bad'
        chans.loc[chans["name"].isin(noisy_ch), "status_description"] = 'noisy'
        chans.loc[chans["name"].isin(flat_ch), "status"] = 'bad'
        chans.loc[chans["name"].isin(flat_ch), "status_description"] = 'flat'
        
        chans.to_csv(bidspath_chans_out.fpath, sep="\t")
        
        np.savetxt(str(bidspath_proc.fpath).replace('_meg.fif','_noisy.txt'),
                   noisy_ch,
                   delimiter = "\n", fmt="%s")
        np.savetxt(str(bidspath_proc.fpath).replace('_meg.fif','_flat.txt'),
                   flat_ch,
                   delimiter = "\n", fmt="%s")


        raw_tsss = mne.preprocessing.maxwell_filter(raw, st_duration=raw.times[-1], coord_frame='meg')
        raw_tsss.save(bidspath_proc, overwrite=True)
        del raw_tsss

        raw.info['bads'] = list(set(noisy_ch + flat_ch))
        raw_tsss_nobad = mne.preprocessing.maxwell_filter(raw, st_duration=raw.times[-1], coord_frame='meg')
        raw_tsss_nobad.save(bidspath_proc_nobad, overwrite=True)
        del raw_tsss_nobad
        
      
print('Maxwell filtering - Done!')
