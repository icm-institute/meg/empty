function [fs] = date_plot(mydate, datasetstoload)

% [fs] = date_plot(mydate, datasetstoload)
%
%   show diagnostic plot with `datasetstoload` datasetes before `mydate`
%
% input: mydate = date string with format "yyyymmdd"
%        datasetstoload = number of datasets to load
%        
% [fs] = date_plot(latedate, earlydate)
%
%   show diagnostic plot all datastes between `earlydate` and `latedate`
%
% input: latedate = date string with format "yyyymmdd"
%        earlydate = date string with format "yyyymmdd"
%        
%%

if ~exist('mydate','var')
    mydate = datestr(now,'yyyymmdd');
end

if ~exist('datasetstoload','var')
    datasetstoload = 30;
end
e_setup
addpath(fullfile(dir_code,'MatlabHelpers/plot'))
addpath(fullfile(dir_code,'MatlabHelpers/plot/panel'))


dir_orig = fullfile(dir_deriv,'mne_maxwell_spec');

% list files in the right folder
alls = flister('(?<sub>sub-[A-Za-z0-9]+)/(?<ses>ses-[A-Za-z0-9]+)/(?<mod>[A-Za-z0-9]+)/\k<sub>_\k<ses>_(?<task>task-[A-Za-z0-9]+)_(?<proc>proc-[A-Za-z0-9]+)_\k<mod>\.(?<ext>[A-Za-z0-9]+)',...
    'dir',dir_orig);

% add date extracted from session
tmp = flister('ses-(?<date>\d{12})(?<manip>[A-Za-z0-9]+)','list',{alls.ses}, 'sortfields','');
[alls.date] = rep2struct(cellfun(@(x)datenum(x,'yyyymmddHHMM'),{tmp.date}));
[alls.manip] = rep2struct(tmp.manip);

alls = sortstruct(alls,'date');

alls = alls([alls.date] < datenum(mydate, 'yyyymmdd'));
if isempty(alls)
    disp('No data to plot (chose a later date)...')
    if nargout 
        fs = [];
    end
    return
end

%% select last days
if ischar(datasetstoload)
    sf = flist_select(alls,'date',datenum(datasetstoload,'yyyymmdd'),'fun',@gt);
    sf = flist_select(sf,'date',datenum(mydate,'yyyymmdd'),'fun',@lt);
    sf_nobad = flist_select(sf,'proc','proc-specnobad');
    sf = flist_select(sf,'proc','proc-spec$');
    datasetstoload = numel(sf);
else
    sf_nobad = flist_select(alls,'proc','proc-specnobad');
    sf_nobad = sf_nobad(end - min([datasetstoload length(sf_nobad)])+1 : end);
    sf = flist_select(alls,'proc','proc-spec$');
    sf = sf(end - min([datasetstoload length(sf)])+1 : end);
end
%% setup output directory
dir_out = fullfile(dir_docs,['diagnostics_' datestr(alls(end).date,'yyyymmddHHMM') '_' num2str(datasetstoload,'%03d')]);
if exist(dir_out, 'dir') && ~isempty(dir(fullfile(dir_out,'*.png')))
    disp('seems like we already have created some plots for this date... skipping.')
%     fs = dir(fullfile(dir_out,'*.png'));
%     for i_f = 1:numel(fs)
%         figure(i_f+3939);clf;
%         imshow(fullfile(fs(i_f).folder,fs(i_f).name));
%     end
    return
end
mymkdir(dir_out)

diagplot(sf, datasetstoload, dir_out, dir_code)
disp('###### done plotting spec')
diagplot(sf_nobad, datasetstoload, dir_out, dir_code)
disp('###### done plotting spec nobad')

function diagplot(sf, datasetstoload, dir_out, dir_code)

freqs = {[20 21.4] [24 25] [48 65]};

saveplots = 1;
gradlabels = textread(fullfile(dir_code,'list_allgradiometers.txt'),'%s\n');
maglabels = textread(fullfile(dir_code,'list_allmagnetometers.txt'),'%s\n');
%%
disp(['============ Diagnostic plot ========='])
disp(['============ loading the ' num2str(datasetstoload) ' recordings before ' datestr(sf(end).date) ' ========='])
big_spec_grad = {};big_spec_mag = {};
% load them
for i_f = 1:numel(sf)
    toread = sf(i_f).name;
    try
        disp(['loading ' myfileparts(toread,'fe')])
        load(toread)
        big_spec_mag{i_f}.f = sf(i_f);
        big_spec_mag{i_f} = spec_mag;
        big_spec_grad{i_f} = spec_grad;
        big_spec_grad{i_f}.f = sf(i_f);
        allfreqs{i_f} = big_spec_mag{i_f}.freq;
    catch
        disp(['failed to load ' toread])
        big_spec_mag{i_f} = [];
        big_spec_grad{i_f} = [];
    end
end
%%
% interpolate frequencies where necessary
disp('interpolating freqs and missing channels where necessary...')
% fill in missing channels with NaN
[m i] = max(cellfun(@numel,allfreqs));
reffreq = allfreqs{i(1)};
for i_f = 1:numel(big_spec_mag)
    if ~isempty(big_spec_mag{i_f}) && ~all(ismember(reffreq, big_spec_mag{i_f}.freq))
        big_spec_mag{i_f}.powspctrm = interp1(big_spec_mag{i_f}.freq,big_spec_mag{i_f}.powspctrm',reffreq)';
        big_spec_grad{i_f}.powspctrm = interp1(big_spec_grad{i_f}.freq,big_spec_grad{i_f}.powspctrm',reffreq)';
        big_spec_mag{i_f}.freq = reffreq;
        big_spec_grad{i_f}.freq = reffreq;
    end
    if numel(big_spec_mag{i_f}.label) ~= numel(maglabels)
        tmp = big_spec_mag{i_f};
        s = size(big_spec_mag{i_f}.powspctrm);
        big_spec_mag{i_f}.powspctrm = NaN([numel(maglabels),s(2:end)]);
        big_spec_mag{i_f}.powspctrm(chnb(tmp.label,maglabels),:) = tmp.powspctrm;
    end
    if numel(big_spec_grad{i_f}.label) ~= numel(gradlabels)
        tmp = big_spec_grad{i_f};
        s = size(big_spec_grad{i_f}.powspctrm);
        big_spec_grad{i_f}.powspctrm = NaN([numel(gradlabels),s(2:end)]);
        big_spec_grad{i_f}.powspctrm(chnb(tmp.label,gradlabels),:) = tmp.powspctrm;
    end
end
template_spec_mag = big_spec_mag{find(cellfun(@(x)~isempty(x),big_spec_mag),1,'last')};

% select non missing ones and catenate in a big matrix
big_spec_mag(emptycells(big_spec_mag)) = [];
cat_spec_mag = cellfun(@(x) x.powspctrm,big_spec_mag,'uniformoutput',0);
cat_spec_mag = cat(3,cat_spec_mag{:});

% retrieve dates of recordings at hand
% dates = cellfun(@(x) regexp(x.cfg.previous.previous.dataset,'/(\d{6})/','tokens'),big_spec_mag);
big_spec_mag{1}.powspctrm = cat_spec_mag;
big_spec_mag = big_spec_mag{1};
big_spec_mag.dimord = 'chan_freq_time';
big_spec_mag.logpowspctrm = log(big_spec_mag.powspctrm);

template_spec_grad = big_spec_grad{find(cellfun(@(x)~isempty(x),big_spec_grad),1,'last')};

% select non missing ones and catenate in a big matrix
% for gradiometers, take module of the 2
big_spec_grad(emptycells(big_spec_grad)) = [];
cat_spec_grad = cellfun(@(x) x.powspctrm,big_spec_grad,'uniformoutput',0);
cat_spec_grad = cat(3,cat_spec_grad{:});
cat_spec_grad = sqrt(cat_spec_grad(chnb('.*2',big_spec_grad{1}.label),:,:).^2 + cat_spec_grad(chnb('.*3',big_spec_grad{1}.label),:,:).^2);

% retrieve dates of recordings at hand
dates = cellfun(@(x) x.f.date,big_spec_grad);
big_spec_grad{1}.powspctrm = cat_spec_grad;
big_spec_grad{1}.label = regexprep(big_spec_grad{1}.label(chnb('.*2',big_spec_grad{1}.label)),'2$','1');
big_spec_grad = big_spec_grad{1};
big_spec_grad.dimord = 'chan_freq_time';
big_spec_grad.logpowspctrm = log(big_spec_grad.powspctrm);

% space out same day recordings
for i = 2:numel(dates)
    while dates(i) <= dates(i-1)
        dates(i) = dates(i)+.3;
    end
end


%% plot

min_spec_mag = template_spec_mag;
min_spec_mag.powspctrm = min(cat_spec_mag,[],1);
min_spec_mag.dimord = 'chan_freq_time';
min_spec_mag.time = 1:numel(dates);
min_spec_mag.label = {'min'};
min_spec_mag.logpowspctrm = log(min_spec_mag.powspctrm);
min_spec_mag.grad = [];
min_spec_mag.cfg = [];

max_spec_mag = template_spec_mag;
max_spec_mag.powspctrm = max(cat_spec_mag,[],1);
max_spec_mag.dimord = 'chan_freq_time';
max_spec_mag.time = 1:numel(dates);
max_spec_mag.label = {'max'};
max_spec_mag.logpowspctrm = log(max_spec_mag.powspctrm);
max_spec_mag.grad = [];
max_spec_mag.cfg = [];

med_spec_mag = template_spec_mag;
med_spec_mag.powspctrm = median(cat_spec_mag,1);
med_spec_mag.mpowspctrm = median(med_spec_mag.powspctrm,3);
med_spec_mag.dimord = 'chan_freq_time';
med_spec_mag.time = 1:numel(dates);
med_spec_mag.label = {'med'};
med_spec_mag.logpowspctrm = log(med_spec_mag.powspctrm);
med_spec_mag.logmpowspctrm = log(med_spec_mag.mpowspctrm);
med_spec_mag.grad = [];
med_spec_mag.cfg = [];

min_spec_grad = template_spec_grad;
min_spec_grad.powspctrm = min(cat_spec_grad,[],1);
min_spec_grad.dimord = 'chan_freq_time';
min_spec_grad.time = 1:numel(dates);
min_spec_grad.label = {'min'};
min_spec_grad.logpowspctrm = log(min_spec_grad.powspctrm);
min_spec_grad.grad = [];
min_spec_grad.cfg = [];

max_spec_grad = template_spec_grad;
max_spec_grad.powspctrm = max(cat_spec_grad,[],1);
max_spec_grad.dimord = 'chan_freq_time';
max_spec_grad.time = 1:numel(dates);
max_spec_grad.label = {'max'};
max_spec_grad.logpowspctrm = log(max_spec_grad.powspctrm);
max_spec_grad.grad = [];
max_spec_grad.cfg = [];

med_spec_grad = template_spec_grad;
med_spec_grad.powspctrm = median(cat_spec_grad,1);
med_spec_grad.mpowspctrm = median(med_spec_grad.powspctrm,3);
med_spec_grad.dimord = 'chan_freq_time';
med_spec_grad.time = 1:numel(dates);
med_spec_grad.label = {'med'};
med_spec_grad.logpowspctrm = log(med_spec_grad.powspctrm);
med_spec_grad.logmpowspctrm = log(med_spec_grad.mpowspctrm);
med_spec_grad.grad = [];
med_spec_grad.cfg = [];



%% plot magnetometers
% spectrum
fig(23,'visible','off');clf;
p = panel();
%     p.margin = [12 10 20 2];
p.margintop = 15;
p.marginbottom = 10;
p.pack('v',{.2 .8})
p(1).pack('h',[.4 .2 .2 .2])
% powspect
p(1,1).select()
% all freqs
% recent days
shadebetween(max_spec_mag.freq,squeeze(max(max_spec_mag.powspctrm(:,:,1:end-1),[],3)),...
    squeeze(min(min_spec_mag.powspctrm(:,:,1:end-1),[],3)),...
    'k','none',.1)
hold on
h(1) = plot(med_spec_mag.freq,squeeze(median(med_spec_mag.powspctrm(:,:,1:end-1),3)),'k');
% today (last one)
shadebetween(max_spec_mag.freq,squeeze(max(max_spec_mag.powspctrm(:,:,end),[],3)),...
    squeeze(min(min_spec_mag.powspctrm(:,:,end),[],3)),...
    'r','none',.2)
hold on
h(2) = plot(med_spec_mag.freq,squeeze(median(med_spec_mag.powspctrm(:,:,end),3)),'r');
axis tight
set(gca,'yscale','log','xscale','log')
legend(h,[num2str(datasetstoload) ' previous recordings'],datestr(sf(end).date,'dd/mm'),'location','southwest');
xlabel('Frequency (Hz)')
p(1,1).ylabel('Power (fT^2)')
p(1,1).title('Powerspectrum (min-med-max)')

% zooms spectrum
for i = 1:3
    p(1,i+1).select();cla
    
    fpts = timepts(freqs{i},med_spec_mag.freq);
    shadebetween(max_spec_mag.freq(fpts),squeeze(max(max_spec_mag.powspctrm(:,fpts,1:end-1),[],3)),...
        squeeze(min(min_spec_mag.powspctrm(:,fpts,1:end-1),[],3)),...
        'k','none',.1)
    hold on
    plot(med_spec_mag.freq(fpts),squeeze(median(med_spec_mag.powspctrm(:,fpts,1:end-1),3)),'color',colors('grey10'))
    shadebetween(max_spec_mag.freq(fpts),squeeze(max(max_spec_mag.powspctrm(:,fpts,end),[],3)),...
        squeeze(min(min_spec_mag.powspctrm(:,fpts,end),[],3)),...
        'r','none',.2)
    plot(med_spec_mag.freq(fpts),squeeze(median(med_spec_mag.powspctrm(:,fpts,end),3)),'r')
    set(gca,'yscale','log','xscale','log')
    axis tight
    title(num2str(freqs{i}));
end
% TFs & topos
p(2).pack('h',3)

timeticks = dates;
for i = 1:3
    cfg = [];
    cfg.channel = {'all'};
    cfg.parameter = 'logpowspctrm';
    cfg.ylim = freqs{i};
    cfg.colorbar = 'no';
    cfg.figure = 'gca';
    
    p(2,i).pack('v',{.5 .4 .1})
    % the TF
    p(2,i,1).pack('v',{.8 []})
    p(2,i,1,1).select()
    
    
    ft_singleplotTFR(cfg,med_spec_mag);
    ylabel('Frequency (Hz)')
    xtick(1:numel(timeticks))
    xticklabel('')
    title('Median power')
    xl = xlim(gca);
    p(2,i,1,1).marginbottom = 0;
    p(2,i,1,2).select()
    p(2,i,1,2).margintop = 0;
    fpts = timepts(cfg.ylim,med_spec_mag.freq);
    toplot = squeeze(mean(mean(med_spec_mag.powspctrm(:,fpts,:),1),2));
    % toplotse = squeeze(std(mean(med_spec_mag.powspctrm,1),[],2));
    plot(med_spec_mag.time,toplot,'marker','o')
    hold on
    % shadebetween(med_spec_mag.time,toplot+toplotse,toplot,'r','none',.2)
    set(gca,'YScale','log')
    xtick(1:numel(timeticks))
    xticklabel('')
    timestrticks = datestr(timeticks,'dd/mm');
    xticklabel(timestrticks,'xticklabelrotation',60)
    yticklabel('')
    xlabel('Date')
    ylabel('Power')
    xlim(xl)
    if numel(timestrticks) > 50
        xx = gca; xx.XAxis.FontSize = 4;
    end
    
    p(2,i,2).pack('h',2)
    % the topos
    % previous topo
    p(2,i,2,1).select()
    
    big_spec_mag.mpowspctrm = median(big_spec_mag.powspctrm(:,:,1:end-1),3);
    big_spec_mag.logmpowspctrm = log(big_spec_mag.mpowspctrm);
    
    cfg = [];
    cfg.parameter = 'logmpowspctrm';
    cfg.comment = 'no';
    cfg.xlim = freqs{i};
    cfg.zlim = 'maxmin';
    cfg.layout = 'neuromag306all.lay';
    cfg.colorbar = 'no';
    cfg.figure = 'gca';
    big_spec_mag.dimord = 'chan_freq';
    ft_topoplotER(cfg,rmfield(big_spec_mag,'cfg'));
    text(-.5,-.5,{'Avg of ' [num2str(datasetstoload) ' previous recordings']},'VerticalAlignment','top');
    
    % today's topo
    p(2,i,2,2).select()
    
    big_spec_mag.mpowspctrm = median(big_spec_mag.powspctrm(:,:,end),3);
    big_spec_mag.logmpowspctrm = log(big_spec_mag.mpowspctrm);
    
    cfg = [];
    cfg.parameter = 'logmpowspctrm';
    cfg.comment = 'no';
    cfg.xlim = freqs{i};
    cfg.zlim = 'maxmin';
    cfg.layout = 'neuromag306all.lay';
    cfg.figure = 'gca';
    big_spec_mag.dimord = 'chan_freq';
    ft_topoplotER(cfg,rmfield(big_spec_mag,'cfg'));
    text(-.5,-.5,datestr(sf(end).date,'dd/mm/yy'));
    set(gca,'clim',get(p(2,i,2,1).axis,'clim'))
    p(2,i,3).select(colorbar('south'))
    
end

if saveplots
    hh = uicontrol('style','text','backgroundcolor',get(gcf,'color'),...
        'units','normalize','string',['Magnetometers ' myfileparts(sf(end).name,'f')],'horizontalalignment','center',...
        'tag','FigTitle','fontsize',14,'horizontalalignment','left','position',[0 .97 1 .03]);
    set(gcf,'paperpositionmode','auto')
    drawnow
    figf = fullfile(dir_out,[datestr(sf(end).date,'yyyymmdd') '_' sf(end).manip '_' sf(end).sub '_' sf(end).proc '_diagnostics_mag.png']);
    disp('=========== Save figure Magnetometers ============')
    
    print('-dpng','-r300',figf)
    disp('Saving png figure --> Done')
    
%     savefig(gcf,strrep(figf,'.png',''),'compact')
%     disp('Saving fig figure --> Done')
    
    close(gcf)
else
    set(gcf,'visible','on')
end

%% plot gradiometers
fig(23,'visible','off');clf;
p = panel();
%     p.margin = [12 10 20 2];
p.margintop = 15;
p.marginbottom = 10;
p.pack('v',{.2 .8})
p(1).pack('h',[.4 .2 .2 .2])
% powspect
p(1,1).select()
% all freqs
% recent days
shadebetween(max_spec_grad.freq,squeeze(max(max_spec_grad.powspctrm(:,:,1:end-1),[],3)),...
    squeeze(min(min_spec_grad.powspctrm(:,:,1:end-1),[],3)),...
    'k','none',.1)
hold on
h = plot(med_spec_grad.freq,squeeze(median(med_spec_grad.powspctrm(:,:,1:end-1),3)),'k');
% today (last one)
shadebetween(max_spec_grad.freq,squeeze(max(max_spec_grad.powspctrm(:,:,end),[],3)),...
    squeeze(min(min_spec_grad.powspctrm(:,:,end),[],3)),...
    'r','none',.2)
hold on
h(2) = plot(med_spec_grad.freq,squeeze(median(med_spec_grad.powspctrm(:,:,end),3)),'r');
axis tight
set(gca,'yscale','log','xscale','log')
legend(h,[num2str(datasetstoload) ' previous recordings'],datestr(sf(end).date,'dd/mm'),'location','southwest');
xlabel('Frequency (Hz)')
p(1,1).ylabel('Power (fT^2)')
p(1,1).title('Powerspectrum (min-med-max)')

% zoom
for i = 1:3
    p(1,i+1).select();cla
    
    fpts = timepts(freqs{i},med_spec_grad.freq);
    shadebetween(max_spec_grad.freq(fpts),squeeze(max(max_spec_grad.powspctrm(:,fpts,1:end-1),[],3)),...
        squeeze(min(min_spec_grad.powspctrm(:,fpts,1:end-1),[],3)),...
        'k','none',.1)
    hold on
    plot(med_spec_grad.freq(fpts),squeeze(median(med_spec_grad.powspctrm(:,fpts,1:end-1),3)),'color',colors('grey10'))
    shadebetween(max_spec_grad.freq(fpts),squeeze(max(max_spec_grad.powspctrm(:,fpts,end),[],3)),...
        squeeze(min(min_spec_grad.powspctrm(:,fpts,end),[],3)),...
        'r','none',.2)
    plot(med_spec_grad.freq(fpts),squeeze(median(med_spec_grad.powspctrm(:,fpts,end),3)),'r')
    set(gca,'yscale','log','xscale','log')
    axis tight
    title(num2str(freqs{i}));
end
% TFs & topos
p(2).pack('h',3)

timeticks = dates;
for i = 1:3
    cfg = [];
    cfg.channel = {'all'};
    cfg.parameter = 'logpowspctrm';
    cfg.ylim = freqs{i};
    cfg.colorbar = 'no';
    cfg.figure = 'gca';
    
    p(2,i).pack('v',{.5 .4 .1})
    % the TF
    p(2,i,1).pack('v',{.8 []})
    p(2,i,1,1).select()
    
    
    ft_singleplotTFR(cfg,med_spec_grad);
    ylabel('Frequency (Hz)')
    xtick(1:numel(timeticks))
    xticklabel('')
    title('Median power')
    xl = xlim(gca);
    p(2,i,1,1).marginbottom = 0;
    p(2,i,1,2).select()
    p(2,i,1,2).margintop = 0;
    fpts = timepts(cfg.ylim,med_spec_grad.freq);
    toplot = squeeze(mean(mean(med_spec_grad.powspctrm(:,fpts,:),1),2));
    % toplotse = squeeze(std(mean(med_spec_grad.powspctrm,1),[],2));
    plot(med_spec_grad.time,toplot,'marker','o')
    hold on
    % shadebetween(med_spec_grad.time,toplot+toplotse,toplot,'r','none',.2)
    set(gca,'YScale','log')
    xtick(1:numel(timeticks))
    xticklabel('')
    timestrticks = datestr(timeticks,'dd/mm');
    xticklabel(timestrticks,'xticklabelrotation',60)
    yticklabel('')
    xlabel('Date')
    ylabel('Power')
    xlim(xl)
    if numel(timestrticks) > 50
        xx = gca; xx.XAxis.FontSize = 4;
    end
    
    p(2,i,2).pack('h',2)
    % the topos
    % previous topo
    p(2,i,2,1).select()
    
    big_spec_grad.mpowspctrm = median(big_spec_grad.powspctrm(:,:,1:end-1),3);
    big_spec_grad.logmpowspctrm = log(big_spec_grad.mpowspctrm);
    
    cfg = [];
    cfg.parameter = 'logmpowspctrm';
    cfg.comment = 'no';
    cfg.xlim = freqs{i};
    cfg.zlim = 'maxmin';
    cfg.layout = 'neuromag306all.lay';
    cfg.colorbar = 'no';
    cfg.figure = 'gca';
    big_spec_grad.dimord = 'chan_freq';
    ft_topoplotER(cfg,rmfield(big_spec_grad,'cfg'));
    text(-.5,-.5,{'Avg of ' [num2str(datasetstoload) ' previous recordings']},'VerticalAlignment','top');
    
    % today's topo
    p(2,i,2,2).select()
    
    big_spec_grad.mpowspctrm = median(big_spec_grad.powspctrm(:,:,end),3);
    big_spec_grad.logmpowspctrm = log(big_spec_grad.mpowspctrm);
    
    cfg = [];
    cfg.parameter = 'logmpowspctrm';
    cfg.comment = 'no';
    cfg.xlim = freqs{i};
    cfg.zlim = 'maxmin';
    cfg.layout = 'neuromag306all.lay';
    cfg.figure = 'gca';
    big_spec_grad.dimord = 'chan_freq';
    ft_topoplotER(cfg,rmfield(big_spec_grad,'cfg'));
    set(gca,'clim',get(p(2,i,2,1).axis,'clim'))
    text(-.5,-.5,datestr(sf(end).date,'dd/mm'));
    p(2,i,3).select(colorbar('south'))
    
end


if saveplots
    hh = uicontrol('style','text','backgroundcolor',get(gcf,'color'),...
        'units','normalize','string',['Gradiometers (module) ' myfileparts(sf(end).name,'f')],'horizontalalignment','center',...
        'tag','FigTitle','fontsize',14,'horizontalalignment','left','position',[0 .97 1 .03]);
    set(gcf,'paperpositionmode','auto')
    drawnow
    figf = fullfile(dir_out,[datestr(sf(end).date,'yyyymmdd') '_' sf(end).manip '_' sf(end).sub '_' sf(end).proc '_diagnostics_grad.png']);
    disp('=========== Save figure Gradiometers ============')
    
    print('-dpng','-r300',figf)
    disp('Saving png figure --> Done')
    
%     savefig(gcf,strrep(figf,'.png',''),'compact')
%     disp('Saving fig figure --> Done')
    
    close(gcf)
else
    set(gcf,'visible','on')
end